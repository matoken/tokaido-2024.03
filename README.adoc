= 東海道らぐ名古屋 2024年3月オフな集まり
春の東海道らぐ的Linuxまつり（画面乱れもあるよ）

日時:: 2024/03/16(sat) 13:00 〜 16:30
会場:: link:https://www.suisin.city.nagoya.jp/system/institution/index.cgi?action=inst_view&inst_key=1164771371[熱田生涯学習センター] & Jitsi Meet
URL:: https://tokaidolug.connpass.com/event/312217/
参加者:: n人

== matoken発表

.GadgetBridge 経由でスマートウォッチで天気，アクティビティ記録
----
$ ls -1 slide/slide.*
slide/slide.adoc <.>
slide/slide.html <.> <.>
slide/slide.pdf <.>
slide/slide.pdf.sig
----

<.> link:slide/slide.adoc[source]
<.> link:slide/slide.html[スライド形式html]
<.> link:slide/slide.pdf[スライド形式pdf]

--


--

== 通信量

----
$ vnstat -h -b '2024-02-18 14:00' -e '2024-02-18 23:00'
----

