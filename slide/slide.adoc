= タブレットがスリープから起きてきて困る
Kenichiro Matohara(matoken) <matoken@kagolug.org>
:revnumber: 1.0
:revdate: 2024-03-16(sat)
:revremark: 「{doctitle}」
:homepage: https://matoken.org/
:imagesdir: resources
:data-uri:
//:example-caption: 例
//table-caption: 表
//figure-caption: 図
:backend: revealjs
:revealjs_theme: solarized
:customcss: resources/my-css.css
:revealjs_slideNumber: c/t
:title-slide-transition: none
:icons: font
:revealjsdir: reveal.js/
:revealjs_hash: true
:revealjs_center: true
:revealjs_autoPlayMedia: true
:revealjs_transition: false
:revealjs_transitionSpeed: fast
:revealjs_plugin_search: enabled

== link:https://matoken.org[Kenichiro Matohara(matoken) https://matoken.org]

image::map.jpg[background, size=cover, width=80%]
* matoken @matoken@inari.opencocon.org
* 鹿児島の右下の山奥から参加
* 好きなLinuxディストリビューションは Debian

map: © OpenStreetMap contributors

== タブレットがスリープから起きてきて困る

* Windows タブレットの Fujitsu ARROWS Tab Q508/SE
** CPU Atom x7-Z8700, RAM4GB, eMMC 128GB, screen 10.1inch/WUXGA(1920×1200)
* Debian 12 bookworm + Xorg + Gnome Shell で利用中
** 「タブレットLinuxで暗号化ファイルシステム(dm-crypt)をアンロック」( link:https://tokaidolug.connpass.com/event/297991/[東海道らぐ名古屋 2023年10月オフな集まり] )  +
https://gitlab.com/matoken/tokaido-2023.10/-/blob/main/slide/slide.adoc

=== !

* 外出時はミニキーボードなどと一緒に持ち運び
* しかし勝手にスリープから復帰してしまう( 1 ~ 数時間毎 )
** 一応復帰後時間経過で再度スリープするがバッテリの減りが速い……
* 家で全く触らずにいても勝手に起きる

== /proc/acpi/wakeup

* `/proc/acpi/wakeup` で各種入力装置からの復帰制御ができるよう
** 例えば laptop で `echo LID0 | sudo tee /proc/acpi/wakeup` とすると LID0 の値が `enabled` -> `disabled` になり、laptop の液晶を開いても復帰しなくなった
* しかしこの端末では `/proc/acpi/wakeup` は空><

[NOTE]
--
参考URL::
link:https://lookbackmargin.blog/2019/06/07/52224703/[Ubuntuがサスペンドから勝手に復帰する問題の解決！ – 回れ右の内輪差 https://lookbackmargin.blog/2019/06/07/52224703/]
--

=== /sys/devices/*/wakeup

----
$ find /sys/devices/ -name wakeup -type f -print
/sys/devices/pnp0/00:03/power/wakeup
/sys/devices/pnp0/00:03/rtc/rtc0/alarmtimer.0.auto/power/wakeup
/sys/devices/platform/INT0002:00/power/wakeup
/sys/devices/platform/ACPI0011:00/gpio-keys.51.auto/power/wakeup
/sys/devices/platform/serial8250/tty/ttyS2/power/wakeup
  :
$ find /sys/devices/ -name wakeup -type f -print | wc -l
29
----

* 結構たくさんある
* `/sys/devices/pnp0/00:03/rtc/rtc0/alarmtimer.0.auto/power/wakeup` が怪しいような?

.無効にしてみる
----
$ echo 'disabled' | sudo tee \
/sys/devices/pnp0/00:03/rtc/rtc0/alarmtimer.0.auto/power/wakeup
$ systemctl suspend
----

やっぱり勝手に起きてくる……

=== 全 wakeup を disabled にしてみる

----
$ find /sys/devices/ -name wakeup -type f -print0 | \
    xargs -0 -I{} sh -c "echo disabled | sudo tee {}"
----

* 放置しているとやっぱり復帰してくる
** 電源ボタンでも復帰できなくなったけど復帰時に `/sys/devices/platform/ACPI0011:00/gpio-keys.51.auto/power/wakeup` を `enabled` で電源ボタン復活

== 何が起こしているのか?

* dmesg を見てもそれらしいログが見当たらない
* `echo 1 | sudo tee /sys/power/pm_debug_messages` して再度試すと `PM` の debug message が出てくるように

.dmesg
----
kernel: Freezing user space processes
kernel: Freezing user space processes completed (elapsed 0.006 seconds)
kernel: OOM killer disabled.
kernel: Freezing remaining freezable tasks                             
kernel: Freezing remaining freezable tasks completed (elapsed 0.002 seconds)  
kernel: PM: Suspending system (s2idle)                                         
kernel: printk: Suspending console(s) (use no_console_suspend to debug)        
kernel: sd 0:0:0:0: [sda] Synchronizing SCSI cache
kernel: PM: suspend of devices complete after 1084.208 msecs
kernel: PM: start suspend of devices complete after 1091.963 msecs
kernel: PM: late suspend of devices complete after 6.550 msecs
kernel: PM: noirq suspend of devices complete after 32.221 msecs       
kernel: PM: suspend-to-idle
----

=== !

----
kernel: Timekeeping suspended for 240.487 seconds       
kernel: PM: Triggering wakeup from IRQ 120
kernel: ACPI: PM: Wakeup unrelated to ACPI SCI
kernel: PM: resume from suspend-to-idle
kernel: PM: noirq resume of devices complete after 28.459 msecs
kernel: PM: early resume of devices complete after 1050.800 msecs
kernel: iwlwifi 0000:01:00.0: Applying debug destination EXTERNAL_DRAM
kernel: iwlwifi 0000:01:00.0: Applying debug destination EXTERNAL_DRAM
kernel: iwlwifi 0000:01:00.0: FW already configured (0) - re-configuring
kernel: PM: resume of devices complete after 600.257 msecs
kernel: PM: Finishing wakeup.                                                  
kernel: OOM killer enabled.                                                    
kernel: Restarting tasks ... done.                                             
kernel: random: crng reseeded on system resumption
kernel: PM: suspend exit
----

=== これかな?

----
kernel: PM: Triggering wakeup from IRQ 120
----

////
$ head -1 /proc/interrupts          
            CPU0       CPU1       CPU2       CPU3
////

.`chv-gpio` ?
----
$ grep 120: /proc/interrupts
 120:        275         23         25         24  chv-gpio    1  ACPI:Event
----

.それらしいこれを disabled にしたら電源ボタンが効かなくなったけど起きてきたよね……
----
$ find /sys/devices/ -name wakeup -type f -print | grep gpio
/sys/devices/platform/ACPI0011:00/gpio-keys.51.auto/power/wakeup
----

////
----
$ sudo find /sys/devices/ -name wakeup -type f -print0 | xargs -n1 -0 -I{} pee "ls -1 {}" "echo disabled | sudo tee {}"
----

----
$ echo enabled | sudo tee /sys/devices/LNXSYSTM:00/LNXSYBUS:00/PNP0C0D:00/power/wakeup
enabled         
----
////

=== /sys/kernel/irq/*/wakeup

.駄目ですorz
----
$ cat /sys/kernel/irq/120/wakeup
enabled
$ echo disabled | sudo tee /sys/kernel/irq/120/wakeup
tee: /sys/kernel/irq/120/wakeup: Permission denied
disabled
----

=== !

----
$ find /sys/kernel/irq/120/ -type f -print | xargs -I{} pee "ls {}" "sudo cat {}"
/sys/kernel/irq/120/wakeup
enabled
/sys/kernel/irq/120/hwirq
1
/sys/kernel/irq/120/actions
ACPI:Event
/sys/kernel/irq/120/type
edge
/sys/kernel/irq/120/chip_name
chv-gpio
/sys/kernel/irq/120/per_cpu_count
395,30,36,32
/sys/kernel/irq/120/name
----

== てな感じで詰まっています

* サスペンドから何もしなくても復帰してくる
* /sys/devices/*/wakeup を全て `disabled` にしても起こる
* log では irq 120 のデバイスがトリガーらしい? -> `chv-gpio` 電源ボタン?
** 電源ボタンが無効になる `/sys/devices/platform/ACPI0011:00/gpio-keys.51.auto/power/wakeup` を `disabled` にしても勝手に復帰する(し電源ボタンで復帰できない)
* 情報募集

== ワークアラウンド?

.バッテリー利用時のサスペンド時間を短めに変更(GUI では15分が最小なので `gsettings` 利用)
----
$ gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-battery-timeout 180
----

.ハイバネーションも使えるのでハイバネーションを使う( 復帰時間は長い )
----
$ cat /sys/power/state
freeze mem disk
$ systemctl hibernate
----

NOTE: これを書いていてハイブリッドスリープの存在を思い出したのでそれでもいけるかもしれない?

== ツッコミなど

* UEFI で s2sleep <-> s3sleep が変更できるかも?
* kernel を変更するとどうだろう?

== 奥付

発表::
* link:https://tokaidolug.connpass.com/event/312217/[東海道らぐ名古屋 2024年3月オフな集まり] 2024-03-16(sat)
発表者::
* link:https://matoken.org/[Kenichiro Matohara(matoken)]
利用ソフトウェア::
* link:https://www.vim.org/[Vim] + link:https://github.com/asciidoctor/asciidoctor-reveal.js[Asciidoctor Reveal.js]
ライセンス::
* link:https://creativecommons.org/licenses/by/4.0/[CC BY 4.0]

